# Enter your code here. Read input from STDIN. Print output to STDOUT     
from collections import OrderedDict
counts = OrderedDict()
n = int(input())
for i in range(n):
    word = input()
    counts[word] = counts.get(word, 0) + 1
print(len(counts))
print(*counts.values())
